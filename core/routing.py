from django.urls import re_path
from . import consumers

""" Websocket routing"""
websocket_urlpatterns = [
    re_path(r'^ws/canvas/(?P<room_name>[^/]+)/$',
            consumers.CanvasConsumer.as_asgi(), name="canvas"),
    re_path(r'^ws/broadcast/(?P<room_name>[^/]+)/$',
            consumers.BroadcastConsumer.as_asgi(), name="broadcast")

]
