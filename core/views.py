from django.views.generic import TemplateView
from django.shortcuts import redirect
from django.urls import reverse
from .utils import generate_room_name, get_websocket_path


class CanvasView(TemplateView):
    template_name = "canvas.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        session_context = self.request.session.get("context")
        if session_context:
            context.update(session_context)
        else:
            redirect('playground')
        return context


class PlaygroundView(TemplateView):
    template_name = "playground.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["playgrounds"] = get_websocket_path()
        return context

    def post(self, request):
        playground = request.POST.get("playground")
        room_name = generate_room_name()

        context = {
            "name": request.POST.get("name"),
            "ws_path": "/ws"+reverse('canvas',  kwargs={"room_id": room_name})+"/"}
        request.session["context"] = context
        return redirect(f"/{playground}/{room_name}")
