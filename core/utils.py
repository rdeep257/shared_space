import random
import string
from .routing import websocket_urlpatterns

def generate_room_name(length=8):
    """Generates a random room name consisting of uppercase letters and digits."""
    chars = string.ascii_uppercase + string.digits
    return ''.join(random.choices(chars, k=length))

def get_websocket_path(name=None):
    if name:
        for i in websocket_urlpatterns:
            if i.name==name:
                return i.path
    else:
        return [i.name for i in websocket_urlpatterns]