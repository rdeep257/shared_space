from channels.generic.websocket import AsyncJsonWebsocketConsumer
USED_COLORS = []

class CanvasConsumer(AsyncJsonWebsocketConsumer):
    """
        Connect to Websocket
    """

    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.username = None
        self.connection_closed = False
        self.room_group_name = 'chat_%s' % self.room_name
        """Join room group"""
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()

    """
          Disconnect Websocket
    """
    async def disconnect(self, close_code):
        self.connection_closed = True
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )
    
    async def close(self, code=None):
        self.connection_closed = True
        return await super().close(code)
    
    async def receive_json(self, ws_data):
        ws_data["channel_name"] = self.channel_name
        await self.channel_layer.group_send(
            self.room_group_name, {"type": "mouse_cordinates", "ws_data": ws_data}
        )

    async def mouse_cordinates(self, event):
        ws_data = event["ws_data"]
        if not self.connection_closed:
            await self.send_json(ws_data)
        else:
            print("Connection already closed")

class BroadcastConsumer(AsyncJsonWebsocketConsumer):
    """
        Connect to Websocket
    """

    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive_json(self, content):
        sender = self.channel_name
        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'message',
                'message': content,
                'sender': sender,
            }
        )

    async def message(self, event):
        message = event['message']
        sender = event['sender']

        if self.channel_name != sender:
            await self.send_json(message)